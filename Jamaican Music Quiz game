# The import required libraries
import random
import statistics
import time
import tkinter as tk
import pandas as pd


#  dictionary of questions and answers
questions = {"Which Jamaican artist had a hit with the song 'One Love'?\na. Jimmy Cliff\nb. Bob Marley\nc. Toots Hibbert\nd. Peter Tosh": "b",
    "Which Jamaican artist had a hit with the song 'Many Rivers to Cross'?\na. Jimmy Cliff\nb. Bob Marley\nc. Toots Hibbert\nd. Peter Tosh": "a",
    "Who is known as the 'King of Dancehall'?\na. Vybz Kartel\nb. Beenie Man\nc. Shabba Ranks\nd. Sean Paul": "a",
    "What is the name of the popular dancehall riddim that features the songs 'Fever'by Vybz Kartel and 'Bruk Off Yuh Back'by Konshens?\na. World Boss\nb. Gyalis Pro\nc. Money Mix\nd. Nuh Fraid": "b",
    "What is the name of the dancehall group that had a hit with the song 'Gal A Bubble'?\na. RDX\nb. Voicemail\nc. T.O.K.\nd. Ward 21":"a",
    "Which Jamaican artist had a hit with the song 'Marcus Garvey'?\na. Burning Spear\nb. Peter Tosh\nc. Bunny Wailer\nd. Bob Marley":"a",
    "Which of the following dancehall artists is known for his hit song 'One Don'?\na.Jahmiel \nb.Squash \nc. Tommy Lee Sparta\nd. Masicka":"b",
    "What is the name of the famous Jamaican song that features the line, 'I remember when we used to sit in the government yard in Trenchtown'?\na. 'Stir It Up'\nb. 'Concrete Jungle'\nc. 'No Woman, No Cry'\nd. 'Jamming'":"b",
    "What is the name of the famous Jamaican song that features the line, 'I shot the sheriff, but I swear it was in self-defense'?\na.'I Shot the Sheriff'\nb.'Get Up, Stand Up'\nc.No Woman, No Cry\nd.'Stir It Up'":"a",
    "Which of the following dancehall artists is known for his hit song 'Badness'?\na. Popcaan\nb.Alkaline\nc. Mavado\nd. Aidonia":"c",
    "What is the name of the famous Jamaican music producer who worked with Bob Marley and helped develop the reggae sound?\na. Lee 'Scratch' Perry\nb. King Jammy\nc. Sly Dunbar\nd. Robbie Shakespeare":"a",
    "Which Jamaican artist had a hit with the song 'Welcome to Jamrock'?\na. Bounty Killer\nb. Capleton\nc. Damian Marley\nd. Buju Banton":"c",
    "Which of the following is NOT a dancehall dance move?\na. Nae Nae\nb. Dutty Wine\nc. Butterfly\nd. Gully Creepa":"a",


}


# function to display a new question
def next_question():
    global previous_question
    question = random.choice(list(questions.keys()))
    while question == previous_question:
        question = random.choice(list(questions.keys()))
    previous_question = question
    question_label.config(text=question)

    if time.time() - start_time >= time_limit:
        feedback_label.config(text=f"Time's up! Your final score is {sum(scores)}.", fg="red")
        submit_button.config(state="disabled")

# set up the game
scores = []
# function to update the score label
def update_score():
    total_score = sum(scores)
    median_score = int(statistics.median(scores))
    average_score = int(sum(scores) / len(scores))
    score_label.config(text=f"Total score: {total_score}\nMedian score: {median_score}\nAverage score: {average_score}")

# function to update the timer label and check for time's up
def update_timer():
    global time_limit, start_time
    elapsed_time = int(time.time() - start_time)
    remaining_time = max(0, time_limit - elapsed_time)
    timer_label.config(text=f"Time left: {remaining_time} sec")
    if remaining_time == 0:
        feedback_label.config(text="Time's up!", fg="red")
        submit_button.config(state="disabled")
        restart_game()
    root.after(1000, update_timer) # update timer every second


previous_question = None
time_limit = 60
start_time = time.time()

# function to check the answer
def check_answer():
    answer = answer_entry.get().strip()
    if answer == questions[previous_question]:
        feedback_label.config(text="Correct!", fg="green")
        scores.append(1)
    elif answer.lower() == "exit":
        feedback_label.config(text="Game over!", fg="red")
        submit_button.config(state="disabled")
        try_again_button.config(state="normal")
        return
    else:
        feedback_label.config(text=f"Incorrect. The correct answer is: {questions[previous_question]}", fg="red")
        scores.append(0)
    update_score()
    next_question()
    answer_entry.delete(0, tk.END)


# function to restart the game
def restart_game():
    global scores
    scores = []
    update_score()
    next_question()
    feedback_label.config(text="")
    submit_button.config(state="normal")
    try_again_button.config(state="normal")

# create the GUI
# root here refers to the main/parent window
root = tk.Tk()
root.title("Jamaican Music Quiz")
root.geometry("500x300")
root.configure(bg="#e0ffff")

# add the widgets
title_label = tk.Label(root, text="Welcome to the Jamaican Music Quiz!", font=("Arial", 16), bg="#e0ffff")
title_label.pack(pady=10)

instructions_label = tk.Label(root, text="Instructions: Answer as many questions as you can within one minute.", bg="#e0ffff")
instructions_label.pack()

question_label = tk.Label(root, text="", font=("Arial", 14), bg="#e0ffff")
question_label.pack(pady=20)

answer_label = tk.Label(root, text="Your answer:", bg="#e0ffff")
answer_label.pack()

answer_entry = tk.Entry(root, width=30)
answer_entry.pack()

submit_button = tk.Button(root, text="Submit", command=check_answer, bg="#ffff00")
submit_button.pack(pady=10)

feedback_label = tk.Label(root, text="", font=("Arial", 12), bg="#e0ffff")
feedback_label.pack()

score_label = tk.Label(root, text="", font=("Arial", 12), bg="#e0ffff")
score_label.pack(pady=10)

timer_label = tk.Label(root, text="Time left: 60 sec", font=("Arial", 16))
timer_label.pack(pady=10)

try_again_button = tk.Button(root, text="Try Again", command= restart_game, bg="#ffff00", state="disabled")
try_again_button.pack(pady=10)

# Create the label for the DataFrame
df_label = tk.Label(root, text=df.to_string(index=False))
df_label.pack()

title_label.config(text=f"Welcome to the Jamaican Music Quiz!")
next_question()
update_score()
update_timer()

# start the GUI loop
root.mainloop()

# create a data frame to store the user's scores
df = pd.DataFrame({'Name': [userid],
                   'Total Score': [sum(scores)],
                   'Median Score': [int(statistics.median(scores))],
                   'Average Score': [int(sum(scores) / len(scores))]})
print(df)
